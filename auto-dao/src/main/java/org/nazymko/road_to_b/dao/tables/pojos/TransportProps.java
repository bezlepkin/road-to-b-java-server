/**
 * This class is generated by jOOQ
 */
package org.nazymko.road_to_b.dao.tables.pojos;


import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.2"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@Entity
@Table(name = "transport_props", schema = "road_to_b_dev")
public class TransportProps implements Serializable {

	private static final long serialVersionUID = -1653899251;

	private Integer   id;
	private Integer   transportId;
	private String    prop;
	private String    value;
	private Boolean   isDisabled;
	private Timestamp changedAt;
	private Timestamp createdAt;

	public TransportProps() {}

	public TransportProps(TransportProps value) {
		this.id = value.id;
		this.transportId = value.transportId;
		this.prop = value.prop;
		this.value = value.value;
		this.isDisabled = value.isDisabled;
		this.changedAt = value.changedAt;
		this.createdAt = value.createdAt;
	}

	public TransportProps(
		Integer   id,
		Integer   transportId,
		String    prop,
		String    value,
		Boolean   isDisabled,
		Timestamp changedAt,
		Timestamp createdAt
	) {
		this.id = id;
		this.transportId = transportId;
		this.prop = prop;
		this.value = value;
		this.isDisabled = isDisabled;
		this.changedAt = changedAt;
		this.createdAt = createdAt;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false, precision = 10)
	@NotNull
	public Integer getId() {
		return this.id;
	}

	public TransportProps setId(Integer id) {
		this.id = id;
		return this;
	}

	@Column(name = "transport_id", nullable = false, precision = 10)
	@NotNull
	public Integer getTransportId() {
		return this.transportId;
	}

	public TransportProps setTransportId(Integer transportId) {
		this.transportId = transportId;
		return this;
	}

	@Column(name = "prop", nullable = false, length = 1024)
	@NotNull
	@Size(max = 1024)
	public String getProp() {
		return this.prop;
	}

	public TransportProps setProp(String prop) {
		this.prop = prop;
		return this;
	}

	@Column(name = "value", nullable = false, length = 1024)
	@NotNull
	@Size(max = 1024)
	public String getValue() {
		return this.value;
	}

	public TransportProps setValue(String value) {
		this.value = value;
		return this;
	}

	@Column(name = "is_disabled", nullable = false)
	@NotNull
	public Boolean getIsDisabled() {
		return this.isDisabled;
	}

	public TransportProps setIsDisabled(Boolean isDisabled) {
		this.isDisabled = isDisabled;
		return this;
	}

	@Column(name = "changed_at", nullable = false)
	@NotNull
	public Timestamp getChangedAt() {
		return this.changedAt;
	}

	public TransportProps setChangedAt(Timestamp changedAt) {
		this.changedAt = changedAt;
		return this;
	}

	@Column(name = "created_at", nullable = false)
	@NotNull
	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public TransportProps setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("TransportProps (");

		sb.append(id);
		sb.append(", ").append(transportId);
		sb.append(", ").append(prop);
		sb.append(", ").append(value);
		sb.append(", ").append(isDisabled);
		sb.append(", ").append(changedAt);
		sb.append(", ").append(createdAt);

		sb.append(")");
		return sb.toString();
	}
}
