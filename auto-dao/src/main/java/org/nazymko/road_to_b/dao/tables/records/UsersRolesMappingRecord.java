/**
 * This class is generated by jOOQ
 */
package org.nazymko.road_to_b.dao.tables.records;


import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;
import org.nazymko.road_to_b.dao.tables.UsersRolesMapping;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * This class is generated by jOOQ.
 */
@Generated(
		value = {
				"http://www.jooq.org",
				"jOOQ version:3.7.2"
		},
		comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
@Entity
@Table(name = "users_roles_mapping", schema = "road_to_b_dev")
public class UsersRolesMappingRecord extends UpdatableRecordImpl<UsersRolesMappingRecord> implements Record3<Integer, Integer, Integer> {

	private static final long serialVersionUID = 1382045523;

	/**
	 * Create a detached UsersRolesMappingRecord
	 */
	public UsersRolesMappingRecord() {
		super(UsersRolesMapping.USERS_ROLES_MAPPING);
	}

	/**
	 * Create a detached, initialised UsersRolesMappingRecord
	 */
	public UsersRolesMappingRecord(Integer id, Integer userId, Integer roleId) {
		super(UsersRolesMapping.USERS_ROLES_MAPPING);

		setValue(0, id);
		setValue(1, userId);
		setValue(2, roleId);
	}

	/**
	 * Getter for <code>road_to_b_dev.users_roles_mapping.id</code>.
	 */
	@Id
	@Column(name = "id", unique = true, nullable = false, precision = 10)
	@NotNull
	public Integer getId() {
		return (Integer) getValue(0);
	}

	/**
	 * Setter for <code>road_to_b_dev.users_roles_mapping.id</code>.
	 */
	public UsersRolesMappingRecord setId(Integer value) {
		setValue(0, value);
		return this;
	}

	/**
	 * Getter for <code>road_to_b_dev.users_roles_mapping.user_id</code>.
	 */
	@Column(name = "user_id", precision = 10)
	public Integer getUserId() {
		return (Integer) getValue(1);
	}

	/**
	 * Setter for <code>road_to_b_dev.users_roles_mapping.user_id</code>.
	 */
	public UsersRolesMappingRecord setUserId(Integer value) {
		setValue(1, value);
		return this;
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * Getter for <code>road_to_b_dev.users_roles_mapping.role_id</code>.
	 */
	@Column(name = "role_id", precision = 10)
	public Integer getRoleId() {
		return (Integer) getValue(2);
	}

	// -------------------------------------------------------------------------
	// Record3 type implementation
	// -------------------------------------------------------------------------

	/**
	 * Setter for <code>road_to_b_dev.users_roles_mapping.role_id</code>.
	 */
	public UsersRolesMappingRecord setRoleId(Integer value) {
		setValue(2, value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<Integer> key() {
		return (Record1) super.key();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row3<Integer, Integer, Integer> fieldsRow() {
		return (Row3) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row3<Integer, Integer, Integer> valuesRow() {
		return (Row3) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field1() {
		return UsersRolesMapping.USERS_ROLES_MAPPING.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field2() {
		return UsersRolesMapping.USERS_ROLES_MAPPING.USER_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<Integer> field3() {
		return UsersRolesMapping.USERS_ROLES_MAPPING.ROLE_ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value2() {
		return getUserId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer value3() {
		return getRoleId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UsersRolesMappingRecord value1(Integer value) {
		setId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UsersRolesMappingRecord value2(Integer value) {
		setUserId(value);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UsersRolesMappingRecord value3(Integer value) {
		setRoleId(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UsersRolesMappingRecord values(Integer value1, Integer value2, Integer value3) {
		value1(value1);
		value2(value2);
		value3(value3);
		return this;
	}
}
