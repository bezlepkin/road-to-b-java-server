/**
 * This class is generated by jOOQ
 */
package org.nazymko.road_to_b.dao.tables;


import org.jooq.*;
import org.jooq.impl.TableImpl;
import org.nazymko.road_to_b.dao.Keys;
import org.nazymko.road_to_b.dao.RoadToBDev;
import org.nazymko.road_to_b.dao.tables.records.DriverCredentialsRecord;

import javax.annotation.Generated;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;


/**
 * Driver security model
 */
@Generated(
		value = {
				"http://www.jooq.org",
				"jOOQ version:3.7.2"
		},
		comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class DriverCredentials extends TableImpl<DriverCredentialsRecord> {

	/**
	 * The reference instance of <code>road_to_b_dev.driver_credentials</code>
	 */
	public static final DriverCredentials DRIVER_CREDENTIALS = new DriverCredentials();
	private static final long serialVersionUID = -1852087651;
	/**
	 * The column <code>road_to_b_dev.driver_credentials.id</code>.
	 */
	public final TableField<DriverCredentialsRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");
	/**
	 * The column <code>road_to_b_dev.driver_credentials.driver_id</code>.
	 */
	public final TableField<DriverCredentialsRecord, Integer> DRIVER_ID = createField("driver_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");
	/**
	 * The column <code>road_to_b_dev.driver_credentials.hash</code>.
	 */
	public final TableField<DriverCredentialsRecord, String> HASH = createField("hash", org.jooq.impl.SQLDataType.VARCHAR.length(512).nullable(false), this, "");
	/**
	 * The column <code>road_to_b_dev.driver_credentials.salt</code>.
	 */
	public final TableField<DriverCredentialsRecord, byte[]> SALT = createField("salt", org.jooq.impl.SQLDataType.BLOB.nullable(false), this, "");
	/**
	 * The column <code>road_to_b_dev.driver_credentials.acces_token</code>.
	 */
	public final TableField<DriverCredentialsRecord, String> ACCES_TOKEN = createField("acces_token", org.jooq.impl.SQLDataType.VARCHAR.length(36), this, "");
	/**
	 * The column <code>road_to_b_dev.driver_credentials.time_to_leave</code>.
	 */
	public final TableField<DriverCredentialsRecord, Timestamp> TIME_TO_LEAVE = createField("time_to_leave", org.jooq.impl.SQLDataType.TIMESTAMP, this, "");

	/**
	 * Create a <code>road_to_b_dev.driver_credentials</code> table reference
	 */
	public DriverCredentials() {
		this("driver_credentials", null);
	}

	/**
	 * Create an aliased <code>road_to_b_dev.driver_credentials</code> table reference
	 */
	public DriverCredentials(String alias) {
		this(alias, DRIVER_CREDENTIALS);
	}

	private DriverCredentials(String alias, Table<DriverCredentialsRecord> aliased) {
		this(alias, aliased, null);
	}

	private DriverCredentials(String alias, Table<DriverCredentialsRecord> aliased, Field<?>[] parameters) {
		super(alias, RoadToBDev.ROAD_TO_B_DEV, aliased, parameters, "Driver security model");
	}

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<DriverCredentialsRecord> getRecordType() {
		return DriverCredentialsRecord.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<DriverCredentialsRecord, Integer> getIdentity() {
		return Keys.IDENTITY_DRIVER_CREDENTIALS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<DriverCredentialsRecord> getPrimaryKey() {
		return Keys.KEY_DRIVER_CREDENTIALS_PRIMARY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<DriverCredentialsRecord>> getKeys() {
		return Arrays.<UniqueKey<DriverCredentialsRecord>>asList(Keys.KEY_DRIVER_CREDENTIALS_PRIMARY, Keys.KEY_DRIVER_CREDENTIALS_DRIVER_IDENTITY_ID_UINDEX);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<DriverCredentialsRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<DriverCredentialsRecord, ?>>asList(Keys.DRIVER_IDENTITY_DRIVER_ID___FK);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DriverCredentials as(String alias) {
		return new DriverCredentials(alias, this);
	}

	/**
	 * Rename this table
	 */
	public DriverCredentials rename(String name) {
		return new DriverCredentials(name, null);
	}
}
