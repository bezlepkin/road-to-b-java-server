package org.nazymko.road_to_b.common.sec.user;

import lombok.Data;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Data
public class UserParams {
    Integer id;
    String phone;
    String name;
}
