package org.nazymko.road_to_b.utils;

import java.util.List;

/**
 * Created by nazymko.patronus@gmail.com
 */
public class ListUtil {
    public static Integer[] toArray(List<Integer> ids) {
        Integer[] array = new Integer[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            array[i] = ids.get(i);
        }
        return array;
    }

}
