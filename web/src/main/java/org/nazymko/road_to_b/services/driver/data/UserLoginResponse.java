package org.nazymko.road_to_b.services.driver.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.nazymko.road_to_b.dao.tables.pojos.Users;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Data
@AllArgsConstructor
public class UserLoginResponse {
    boolean found;
    String token;
    Users driver;
}
