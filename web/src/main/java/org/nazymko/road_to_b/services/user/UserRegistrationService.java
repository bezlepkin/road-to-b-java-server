package org.nazymko.road_to_b.services.user;

import lombok.extern.log4j.Log4j2;
import org.nazymko.dao.UserCredentialsDao;
import org.nazymko.dao.UserRolesDao;
import org.nazymko.dao.UserRolesMappingDao;
import org.nazymko.road_to_b.common.sec.user.UserParams;
import org.nazymko.road_to_b.dao.tables.pojos.*;
import org.nazymko.road_to_b.dao.tables.records.UserCredentialsRecord;
import org.nazymko.road_to_b.utils.TimestampUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.nazymko.road_to_b.dao.Tables.USERS;
import static org.nazymko.road_to_b.dao.Tables.USER_IDENTITIES;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Log4j2
@Service
public class UserRegistrationService extends AbstractService {

    @Resource
    UserRolesDao userRolesDao;
    @Resource
    UserRolesMappingDao userRolesMappingDao;
    @Resource
    UserCredentialsDao userCredentialsDao;

    public Users find(UserParams params) {
        UserIdentities userIdentities = getIdentitiesDao().fetchOne(USER_IDENTITIES.PHONE, params.getPhone());
        if (userIdentities != null) {
            return findUser(userIdentities.getUserId());
        } else {
            return null;
        }
    }

    public Map<String, Object> login(UserParams params) {
        Users users = find(params);
        HashMap<String, Object> result = new HashMap<>();
        if (users != null) {
            UserCredentialsRecord credentials = userCredentialsDao.findByUserId(users.getId()).get();

            result.put("userId", credentials.getUserId());
            result.put("accessToken", credentials.getAccesToken());

        } else {
            result.put("message", "User Not Found.");
        }
        return result;
    }

    public Users register(UserParams params) {

        UserIdentities userIdentities = getIdentitiesDao().fetchOne(USER_IDENTITIES.PHONE, params.getPhone());
        if (userIdentities == null) {
            return reg(params);
        } else {
            return find(params);
        }
    }


    /*Add transactions*/
    private Users reg(UserParams params) {
        Integer id = saveUser(params);
        saveIdentity(params, id);
        saveContacts(params, id);
        saveCredentials(id);
        addDefaultRole(id);
        return findUser(id);
    }

    private void saveCredentials(Integer userId) {
        UserCredentials credentials = new UserCredentials();
        credentials.setUserId(userId);
        credentials.setAccesToken(UUID.randomUUID().toString());
        getUserCredentials().insert(credentials);

    }

    private void addDefaultRole(Integer id) {
        List<UserRoles> defaultRoles = userRolesDao.getDefaultRoles();
        userRolesMappingDao.assignDefaultRoles(id, defaultRoles);
    }

    private Users findUser(Integer id) {
        return getUsersDao().findById(id);
    }

    private void saveContacts(UserParams params, Integer id) {
        UserContacts contact = new UserContacts();
        contact.setUserId(id)
                .setProp("phone")
                .setValue(params.getPhone())
                .setIsDisabled(false);
        getUserContacts()
                .insert(contact);
    }

    private void saveIdentity(UserParams params, Integer id) {
        UserIdentities identity = new UserIdentities();
        identity.setUserId(id)
                .setPhone(params.getPhone());
        getIdentitiesDao().insert(identity);
    }

    private Integer saveUser(UserParams params) {
        Timestamp now = TimestampUtils.now();
        Users user = new Users()
                .setName(params.getName())
                .setXIdentity(now.getTime())
                .setIsDisabled(false)
                .setCreatedAt(now);
        getUsersDao().insert(user);
        /*Need to rework it with DSL*/
        Users users = getUsersDao().fetchOne(USERS.X_IDENTITY, now.getTime());

        return users.getId();

    }

}
