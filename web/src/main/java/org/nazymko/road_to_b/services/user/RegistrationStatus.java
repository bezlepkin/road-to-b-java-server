package org.nazymko.road_to_b.services.user;

/**
 * Created by nazymko.patronus@gmail.com
 */
public enum RegistrationStatus {
    OK, FAILED
}
