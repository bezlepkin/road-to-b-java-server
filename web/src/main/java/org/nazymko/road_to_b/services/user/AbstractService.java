package org.nazymko.road_to_b.services.user;

import lombok.Getter;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.nazymko.dao.UserCredentialsDao;
import org.nazymko.road_to_b.dao.tables.daos.OrdersDao;
import org.nazymko.road_to_b.dao.tables.daos.UserContactsDao;
import org.nazymko.road_to_b.dao.tables.daos.UserIdentitiesDao;
import org.nazymko.road_to_b.dao.tables.daos.UsersDao;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by nazymko.patronus@gmail.com
 */
@org.springframework.stereotype.Service
public abstract class AbstractService {
    @Getter
    @Resource
    DataSource dataSource;
    @Getter
    @Resource
    UsersDao usersDao;
    @Getter
    @Resource
    UserContactsDao userContacts;
    @Getter
    @Resource
    UserIdentitiesDao identitiesDao;
    @Resource
    OrdersDao ordersDao;
    @Getter
    @Resource
    UserCredentialsDao userCredentials;


    public DSLContext getDsl() {
        return DSL.using(dataSource, SQLDialect.MYSQL);
    }

}
