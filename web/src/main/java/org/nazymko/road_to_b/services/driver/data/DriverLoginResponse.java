package org.nazymko.road_to_b.services.driver.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.nazymko.road_to_b.dao.tables.pojos.Drivers;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Data
@AllArgsConstructor
public class DriverLoginResponse {
    boolean found;
    String token;
    Drivers driver;
}
