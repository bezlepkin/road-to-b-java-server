package org.nazymko.road_to_b.services.search;

import lombok.Data;

import java.sql.Timestamp;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Data
public class DateBounce {
    Timestamp startDate;
    Timestamp endDate;
}
