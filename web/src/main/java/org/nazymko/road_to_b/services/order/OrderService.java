package org.nazymko.road_to_b.services.order;

import org.nazymko.road_to_b.dao.tables.pojos.Orders;
import org.springframework.stereotype.Service;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Service("orderService")
public class OrderService {


    public void addOrder(Orders order) {
    }

    public void deleteOrder(Integer tripId, Integer orderId) {
    }

    public void ordersForTrip(Integer tripId) {
    }


}
