package org.nazymko.road_to_b.services.driver;

import org.nazymko.road_to_b.dao.tables.pojos.Drivers;
import org.nazymko.road_to_b.services.driver.data.GeneralRegistrationRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Service

public class RegistrationService {

    @Resource
    DriverSecurityService securityService;

    public Optional<Drivers> register(GeneralRegistrationRequest userData) throws NoSuchAlgorithmException {
        return securityService.register(userData);
    }
}
