package org.nazymko.road_to_b.services.driver;

import org.nazymko.road_to_b.services.driver.data.DriverLoginResponse;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Service
public class LoginService {
    @Resource
    DriverSecurityService securityService;


    public DriverLoginResponse doLogin(String login, String driverPassword) {
        return securityService.login(login, driverPassword);
    }

}
