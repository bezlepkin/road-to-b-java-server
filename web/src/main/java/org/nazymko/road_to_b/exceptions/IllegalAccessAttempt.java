package org.nazymko.road_to_b.exceptions;

import lombok.Getter;

/**
 * Created by nazymko.patronus@gmail.com
 */

public class IllegalAccessAttempt extends RuntimeException {
    @Getter
    String path;
    @Getter
    Object params;
    @Getter
    Object headers;

    public IllegalAccessAttempt(String path, Object params, Object headers) {
        super("Authorization needed :" + path);

        this.path = path;
        this.params = params;
        this.headers = headers;
    }

}
