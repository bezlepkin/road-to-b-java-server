package org.nazymko.road_to_b.exceptions.data;

import lombok.Data;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Data
public class ErrorObject {
    private final String path;
    private final Object headers;
    private final Object params;
    private String message;

    public ErrorObject(String path, Object headers, Object params, String message) {
        this.path = path;
        this.headers = headers;
        this.params = params;
        this.message = message;
    }
}
