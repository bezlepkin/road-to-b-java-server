package org.nazymko.road_to_b.api.v1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by nazymko.patronus@gmail.com.
 */
@Controller
@RequestMapping("api")
public class InfoController {
    @RequestMapping("version")
    @ResponseBody
    public String version() {
        return "1";
    }
}
