package org.nazymko.road_to_b.api.v1;

import org.nazymko.dao.*;
import org.nazymko.road_to_b.dao.tables.pojos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by nazymko.patronus@gmail.com.
 */
@Controller
@RequestMapping({"/api/v1/trips", "/api/latest/trips"})
public class TripController {
    @Autowired
    TripDao tripDao;
    @Resource
    RoutineDao routineDao;
    @Resource
    RoutPointMappingsDao routPointMappingsDao;
    @Resource
    PointDao pointDao;
    @Resource
    SitsDao sitsDao;
    @Resource
    OrderDao orderDao;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public Object all(Model model) {
        return tripDao.fetchByIsDisabled(false);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Object register(Model model) {
        throw new NotImplementedException();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public Trips registerGet(@PathVariable Integer id) {
        return tripDao.fetchOneById(id);
    }

    @ResponseBody
    @RequestMapping(value = "{id}/routine/details", method = RequestMethod.GET)
    public HashMap<String, Object> getRoutines(@PathVariable Integer id) {
        Integer routineId = tripDao.fetchOneById(id).getRoutineId();
        Routines routine = routineDao.fetchOneById(routineId);
        List<RoutPointMappings> pointMapping = routPointMappingsDao.fetchByRoutineId(routine.getId());

        List<Points> detailPoints = new LinkedList<Points>();

        for (RoutPointMappings routPointMappings : pointMapping) {
            Points point = pointDao.fetchOneById(routPointMappings.getPointId());
            detailPoints.add(point);
        }

        HashMap<String, Object> result = new HashMap<>();
        result.put("id", routineId);
        HashMap<Object, Object> resultRoutine = new HashMap<>();
        result.put("routine", resultRoutine);

        resultRoutine.put("details", routine);
        resultRoutine.put("points", pointMapping);

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "{id}/routine", method = RequestMethod.GET)
    public Routines getRoutine(@PathVariable Integer id) {
        Integer routineId = tripDao.fetchOneById(id).getRoutineId();
        return routineDao.fetchOneById(routineId);
    }

    @ResponseBody
    @RequestMapping(value = {"{id}/sits", "{id}/sits/all"}, method = RequestMethod.GET)
    public List<Sits> sits(@PathVariable Integer id) {
        Trips trip = tripDao.fetchOneById(id);
        List<Sits> sits = sitsDao.fetchByTripId(trip.getId());
        return sits;
    }

    @ResponseBody
    @RequestMapping(value = "{id}/sits/free", method = RequestMethod.GET)
    public List<Sits> available(@PathVariable Integer id) {
        Trips trip = tripDao.fetchOneById(id);
        List<Sits> sits = sitsDao.fetchByTripId(trip.getId());
        List<Orders> orders = orderDao.fetchByTripId(id);
        for (Orders order : orders) {
            sits.removeIf(x -> order.getSitId().equals(x.getId()));
        }
        return sits;
    }


}
