package org.nazymko.road_to_b.api.v1;

import org.nazymko.dao.PointDao;
import org.nazymko.dao.RoutPointMappingsDao;
import org.nazymko.dao.TripDao;
import org.nazymko.road_to_b.dao.tables.pojos.Points;
import org.nazymko.road_to_b.dao.tables.pojos.Trips;
import org.nazymko.road_to_b.services.point.PointService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;


/**
 * Created by nazymko.patronus@gmail.com.
 */
@RestController
@RequestMapping({"/api/v1/points", "/api/latest/points"})
public class PointsController {
    private static final int START = 0;
    @Resource
    PointDao pointDao;
    @Resource
    TripDao tripDao;

    @Resource
    RoutPointMappingsDao routPointMappingsDao;
    @Resource
    PointService pointService;

    @RequestMapping(method = RequestMethod.POST)
    public Object register(Model model) {
        throw new NotImplementedException();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Points registerGet(@PathVariable Long id) {
        return pointDao.fetchOneById(id);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<Points> all() {
        return pointDao.findAll();
    }

    @RequestMapping(value = "{id}/trips", method = RequestMethod.GET)
    public List<Trips> routes(@PathVariable Long id) {
        return pointService.tripsFromPoint(id);
    }


    @RequestMapping(value = "all/names", method = RequestMethod.GET)
    public HashMap<Long, String> allAsMap() {
        List<Points> all = pointService.all();
        HashMap<Long, String> names = new HashMap<>();
        for (Points points : all) {
            names.put(points.getId(), points.getName());
        }
        return names;
    }

    @RequestMapping(value = "all/names/reverted", method = RequestMethod.GET)
    public HashMap<String, Long> allAsRevertedMap() {
        List<Points> all = pointService.all();
        HashMap<String, Long> names = new HashMap<>();
        for (Points points : all) {
            names.put(points.getName(), points.getId());
        }
        return names;
    }
}
