package org.nazymko.road_to_b.api.v1;

import org.nazymko.dao.TripDao;
import org.nazymko.road_to_b.dao.tables.pojos.Trips;
import org.nazymko.road_to_b.services.search.DateBounce;
import org.nazymko.road_to_b.utils.TimestampUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.nazymko.road_to_b.dao.Tables.TRIPS;

/**
 * Created by nazymko.patronus@gmail.com
 */
@Controller
@RequestMapping({"/api/v1/trips/search", "/api/latest/trips/search"})
public class SearchController {
    @Resource
    TripDao tripDao;
    //    @Resource
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    @ResponseBody
    @RequestMapping(value = "basic", method = RequestMethod.GET)
    public List<Trips> basicSearchByTwoPoints(@RequestParam(value = "from") Long fromPoint,
                                              @RequestParam(value = "to") Long toPoint,
                                              @RequestParam(value = "on", required = false) String on,
                                              @RequestParam(value = "before", required = false, defaultValue = "31-12-2100") String before,
                                              @RequestParam(value = "after", required = false, defaultValue = "01-01-2016") String after,
                                              @RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
                                              @RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize) throws ParseException {

        DateBounce dateBounce = makeBounce(on, after, before);

        return tripDao.getDsl().selectFrom(TRIPS)
                .where(TRIPS.DEPARTURE_POINT.eq(fromPoint))
                .and(TRIPS.ARRIVAL_POINT.eq(toPoint))
                .and(TRIPS.DEPARTURE_DATE.between(
                        dateBounce.getStartDate(),
                        dateBounce.getEndDate()))
                .limit(page * pageSize, pageSize)
                .fetch().into(Trips.class);


    }


    private DateBounce makeBounce(String on, String after, String before) throws ParseException {

        DateBounce bounce = new DateBounce();

        if (on != null) {
            Date onDate = dateFormatter.parse(on);

            bounce.setStartDate(onDay(onDate));
            bounce.setEndDate(dayAfter(onDate));
        } else {
            Date endDate = dateFormatter.parse(before);
            Date startDate = dateFormatter.parse(after);

            bounce.setStartDate(onDay(startDate));
            bounce.setEndDate(dayAfter(endDate));
        }


        return bounce;
    }

    private Timestamp onDay(Date onDate) {
        return TimestampUtils.from(onDate.toInstant());
    }

    private Timestamp dayAfter(Date onDate) {
        return TimestampUtils.from(onDate.toInstant().plus(1, ChronoUnit.DAYS));
    }
}
