package org.nazymko.road_to_b.api.v1;

import org.nazymko.dao.DriverDao;
import org.nazymko.dao.DriverPropsDao;
import org.nazymko.dao.DriverTypesDao;
import org.nazymko.dao.TripDao;
import org.nazymko.road_to_b.dao.tables.pojos.DriverProps;
import org.nazymko.road_to_b.dao.tables.pojos.DriverTypes;
import org.nazymko.road_to_b.dao.tables.pojos.Drivers;
import org.nazymko.road_to_b.dao.tables.pojos.Trips;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;


/**
 * Created by nazymko.patronus@gmail.com.
 */
@Controller
@RequestMapping({"/api/v1/drivers", "/api/latest/drivers"})
public class DriverController {
    @Autowired
    TripDao tripDao;

    @Autowired
    DriverDao driverDao;
    @Resource
    DriverPropsDao driverPropsDao;

    @RequestMapping(method = RequestMethod.POST)
    public Object register(Model model) {
        throw new NotImplementedException();
    }

    @ResponseBody
    @RequestMapping(value = "{id}/trips", method = RequestMethod.GET)
    public List<Trips> getTrips(@PathVariable Integer id) {
        return tripDao.fetchByDriverId(id);
    }

    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Drivers driver(@PathVariable Integer id) {
        return driverDao.fetchOneById(id);
    }

    @Resource
    DriverTypesDao driverTypesDao;

    @ResponseBody
    @RequestMapping(value = "{id}/details", method = RequestMethod.GET)
    public HashMap<String, Object> driverDetails(@PathVariable Integer id) {
        Drivers driver = driverDao.fetchOneById(id);
        List<DriverProps> driverPropses = driverPropsDao.fetchByDriverId(id);
        DriverTypes driverTypes = driverTypesDao.fetchOneById(driver.getDriverType());

        HashMap<String, Object> result = new HashMap<>();

        result.put("id", id);
        result.put("driver", driver);
        result.put("details", driverPropses);
        result.put("type", driverTypes);
        return result;
    }

}
