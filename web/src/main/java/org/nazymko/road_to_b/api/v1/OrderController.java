package org.nazymko.road_to_b.api.v1;

import org.nazymko.dao.OrderDao;
import org.nazymko.dao.TripDao;
import org.nazymko.dao.UserCredentialsDao;
import org.nazymko.road_to_b.dao.tables.pojos.Orders;
import org.nazymko.road_to_b.dao.tables.pojos.Trips;
import org.nazymko.road_to_b.exceptions.IllegalAccessAttempt;
import org.nazymko.road_to_b.exceptions.data.ErrorObject;
import org.nazymko.road_to_b.services.driver.DriverSecurityService;
import org.nazymko.road_to_b.services.order.OrderService;
import org.nazymko.road_to_b.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;


/**
 * Created by nazymko.patronus@gmail.com.
 */
@Controller
@RequestMapping({OrderController.API_PATH, OrderController.API_LATEST_ORDERS})
public class OrderController {
    public static final String API_PATH = "/api/v1/orders";
    public static final String API_LATEST_ORDERS = "/api/latest/orders";
    @Autowired
    OrderDao orderDao;
    @Resource
    TripDao tripDao;

    @Resource
    DriverSecurityService driverService;

    @Resource
    OrderService orderService;
    @Resource
    UserCredentialsDao userCredentialsDao;

    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Orders registerGet(@PathVariable Integer id) {
        return orderDao.fetchOneById(id);
    }

    @ResponseBody
    @RequestMapping(value = "{id}/trip", method = RequestMethod.GET)
    public Trips orderTrip(@PathVariable Integer id) {
        Orders order = orderDao.fetchOneById(id);
        Trips trip = tripDao.fetchOneById(order.getTripId());
        return trip;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public Orders addOrder(@RequestBody Orders order, @RequestHeader("X-Access-Token") String accessToken) {

        if (!driverService.hasAcces(order.getUserId(), accessToken)) {
            /*TODO: better response*/
            throwException(order, accessToken);

        }
        orderDao.store(order);
        return order;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    @RequestMapping(method = RequestMethod.DELETE)
    public HashMap rejectOrder(@RequestBody Orders order, @RequestHeader("X-Access-Token") String accessToken) {
        /*TODO: Move in user service*/
        if (driverService.hasAcces(order.getUserId(), accessToken)) {
            orderDao.deleteById(order.getId());
        } else {
            throwException(order, accessToken);
        }
        return Response.make().put("message", "deleted").data();
    }

    private void throwException(@RequestBody Orders order, @RequestHeader("X-Access-Token") String accessToken) {
    /*TODO: better response*/
        HashMap<String, String> headers = new HashMap<>();
        headers.put("accessToken", accessToken);
        throw new IllegalAccessAttempt(API_PATH, order, headers);
    }


    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(IllegalAccessAttempt.class)
    Object handleException(IllegalAccessAttempt ex) {
        return new ErrorObject(ex.getPath(), ex.getHeaders(), ex.getParams(), ex.getMessage());
    }

}
