package org.nazymko.road_to_b.api.v1;

import org.nazymko.road_to_b.dao.tables.daos.TransportsDao;
import org.nazymko.road_to_b.dao.tables.pojos.Transports;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


/**
 * Created by nazymko.patronus@gmail.com.
 */
@Controller
@RequestMapping({"/api/v1/transports","/api/latest/transports"})
public class TransportController {
    @Autowired
    TransportsDao transportDao;

    @RequestMapping(method = RequestMethod.POST)
    public Object register(Model model) {
        throw new NotImplementedException();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public Transports registerGet(@PathVariable Integer id) {
        return transportDao.fetchOneById(id);
    }


}
