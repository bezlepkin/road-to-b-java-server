package org.nazymko.dao;

import org.nazymko.road_to_b.dao.tables.pojos.UserRoles;
import org.nazymko.road_to_b.dao.tables.pojos.UsersRolesMapping;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.nazymko.road_to_b.dao.Tables.USERS_ROLES_MAPPING;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class UserRolesMappingDao extends org.nazymko.road_to_b.dao.tables.daos.UsersRolesMappingDao implements ContextSupply {
    @Resource
    DataSource dataSource;
    @Resource
    UserRolesDao userRolesDao;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }


    public List<UserRoles> getUserRoles(Integer userId) {
        List<UsersRolesMapping> usersRolesMappings = fetchByUserId(userId);
        Integer[] roles = new Integer[usersRolesMappings.size()];
        usersRolesMappings.forEach(new Consumer<UsersRolesMapping>() {
            int i = 0;

            @Override
            public void accept(UsersRolesMapping x) {
                roles[i] = x.getRoleId();
            }
        });

        return userRolesDao.fetchById(roles);
    }

    public List<UsersRolesMapping> getUserRoleIds(Integer userId) {
        return fetchByUserId(userId);
    }


    public void assignDefaultRoles(Integer userId, List<UserRoles> roles) {
        int count = getDsl().fetchCount(getDsl()
                .selectFrom(USERS_ROLES_MAPPING).where(USERS_ROLES_MAPPING.USER_ID.eq(userId)));
        if (count > 0) {
            throw new IllegalStateException("Trying initialize existing user. User id = " + userId);
        }
        assignRolesWithoutCheck(userId, roles);


    }

    private void assignRolesWithoutCheck(Integer userId, List<UserRoles> roles) {
    /*Add new roles*/
        List<UsersRolesMapping> permissions = new ArrayList<>();

        for (UserRoles role : roles) {
            UsersRolesMapping userRole = new UsersRolesMapping().setRoleId(role.getId()).setUserId(userId);
            permissions.add(userRole);
        }

        insert(permissions);
    }

}
