package org.nazymko.dao;

import org.nazymko.road_to_b.dao.tables.DriverTypes;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class DriverTypesDao extends org.nazymko.road_to_b.dao.tables.daos.DriverTypesDao implements ContextSupply {
    @Resource
    DataSource dataSource;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }


    public Integer regular() {
        org.nazymko.road_to_b.dao.tables.pojos.DriverTypes one = fetchOne(DriverTypes.DRIVER_TYPES.NAME, "regular");

        if (one == null) {
            throw new IllegalStateException("Could'not find driver type 'regular'");
        }
        return one.getId();
    }
}
