package org.nazymko.dao;

import org.nazymko.road_to_b.dao.tables.records.DriverCredentialsRecord;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Optional;

import static org.nazymko.road_to_b.dao.tables.DriverCredentials.DRIVER_CREDENTIALS;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class DriverCredentialsDao extends org.nazymko.road_to_b.dao.tables.daos.DriverCredentialsDao implements ContextSupply {
    @Resource
    DataSource dataSource;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }


    public void updateToken(String token, Integer driverId) {
        getDsl().update(DRIVER_CREDENTIALS)
                .set(DRIVER_CREDENTIALS.ACCES_TOKEN, token)
                .where(DRIVER_CREDENTIALS.DRIVER_ID.eq(driverId))
                .execute();

    }

    public Optional<DriverCredentialsRecord> finByDriverId(Integer driverId) {
        DriverCredentialsRecord driverCredentialsRecord = getDsl()
                .selectFrom(DRIVER_CREDENTIALS)
                .where(DRIVER_CREDENTIALS.DRIVER_ID.eq(driverId))
                .fetchOne();

        return Optional.ofNullable(driverCredentialsRecord);

    }
}
