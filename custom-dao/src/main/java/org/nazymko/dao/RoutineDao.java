package org.nazymko.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class RoutineDao extends org.nazymko.road_to_b.dao.tables.daos.RoutinesDao implements ContextSupply {
    @Resource
    DataSource dataSource;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }
}
