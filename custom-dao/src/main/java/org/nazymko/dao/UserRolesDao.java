package org.nazymko.dao;

import org.nazymko.road_to_b.dao.tables.pojos.UserRoles;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.List;

/**
 * Created by nazymko.patronus@gmail.com.
 */
public class UserRolesDao extends org.nazymko.road_to_b.dao.tables.daos.UserRolesDao implements ContextSupply {
    @Resource
    DataSource dataSource;

    @Override
    public DataSource getDatasource() {
        return dataSource;
    }

    public List<UserRoles> getDefaultRoles() {
        return fetchByIsDefault(true);
    }
}
