SET  FOREIGN_KEY_CHECKS =0;
ALTER TABLE road_to_b_dev.trips ADD price DOUBLE NOT NULL DEFAULT 0;
ALTER TABLE road_to_b_dev.trips ADD price_currency VARCHAR(16) NULL DEFAULT 'RYB';
SET  FOREIGN_KEY_CHECKS =1;