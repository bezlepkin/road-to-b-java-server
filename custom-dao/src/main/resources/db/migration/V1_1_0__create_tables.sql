SET FOREIGN_KEY_CHECKS = 0;
CREATE TABLE driver
(
  id          INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  first_name  VARCHAR(1024)                       NOT NULL,
  last_name   VARCHAR(1024)                       NOT NULL,
  is_disabled TINYINT DEFAULT 0                   NOT NULL,
  driver_type INT                                 NOT NULL,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE driver_props
(
  id          INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  driver_id   INT                                 NOT NULL,
  prop        VARCHAR(1024)                       NOT NULL,
  value       VARCHAR(1024)                       NOT NULL,
  is_disabled TINYINT                             NOT NULL,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE driver_types
(
  id          INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  description VARCHAR(1024)                       NOT NULL,
  name        VARCHAR(1024)                       NOT NULL,
  is_disabled TINYINT                             NOT NULL,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE orders
(
  id          INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  sit_id      INT                                 NOT NULL,
  trip_id     INT                                 NOT NULL,
  user_id     INT                                 NOT NULL,
  is_disabled TINYINT                             NOT NULL,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE orders_status
(
  id         INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  order_id   INT                                 NOT NULL,
  driver_id  INT                                 NOT NULL,
  status_id  INT                                 NOT NULL,
  message    LONGTEXT                            NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  changed_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE orders_status_mapping
(
  id         INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  name       VARCHAR(128)                        NOT NULL,
  changed_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE point
(
  id          BIGINT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
  name        LONGTEXT                            NOT NULL,
  longitude   DOUBLE                              NOT NULL,
  latitude    DOUBLE                              NOT NULL,
  is_disabled TINYINT                             NOT NULL,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE rout_point_mapping
(
  routine_id  INT                                 NOT NULL,
  point_id    BIGINT                              NOT NULL,
  ordering    INT                                 NOT NULL,
  is_disabled TINYINT                             NOT NULL,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL

  #   ,created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE routine
(
  id          INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  description VARCHAR(1024)                       NOT NULL,
  is_disabled TINYINT                             NOT NULL,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE sits
(
  id           INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  transport_id INT                                 NOT NULL,
  trip_id      INT                                 NOT NULL,
  place_number SMALLINT                            NOT NULL,
  is_disabled  TINYINT                             NOT NULL,
  changed_at   TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at   TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE transport
(
  id          INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  name        VARCHAR(1024)                       NOT NULL,
  is_disabled TINYINT                             NOT NULL,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE transport_props
(
  id           INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  transport_id INT                                 NOT NULL,
  prop         VARCHAR(1024)                       NOT NULL,
  value        VARCHAR(1024)                       NOT NULL,
  is_disabled  TINYINT DEFAULT 0                   NOT NULL,
  changed_at   TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at   TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE trip
(
  id             INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  transport_id   INT                                 NOT NULL,
  driver_id      INT                                 NOT NULL,
  departure_date DATETIME                            NOT NULL,
  arrival_date   TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  is_disabled    TINYINT                             NOT NULL,
  routine_id     INT                                 NOT NULL,
  changed_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE user
(
  id          INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  name        VARCHAR(1024)                       NOT NULL,
  is_disabled TINYINT,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE user_contact
(
  id          INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  user_id     INT                                 NOT NULL,
  prop        VARCHAR(128)                        NOT NULL,
  value       VARCHAR(1024)                       NOT NULL,
  is_disabled TINYINT,
  changed_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
CREATE TABLE user_identity
(
  id         INT PRIMARY KEY                     NOT NULL AUTO_INCREMENT,
  user_id    INT                                 NOT NULL,
  phone      VARCHAR(32)                         NOT NULL,
  changed_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
ALTER TABLE driver
  ADD FOREIGN KEY (driver_type) REFERENCES driver_types (id);
ALTER TABLE driver_props
  ADD FOREIGN KEY (driver_id) REFERENCES driver (id);
CREATE UNIQUE INDEX unique_id ON driver_types (id);
ALTER TABLE orders
  ADD FOREIGN KEY (sit_id) REFERENCES sits (id);
ALTER TABLE orders
  ADD FOREIGN KEY (trip_id) REFERENCES trip (id);
ALTER TABLE orders
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
CREATE UNIQUE INDEX unique_id ON orders (id);
ALTER TABLE orders_status
  ADD FOREIGN KEY (order_id) REFERENCES orders (id);
ALTER TABLE orders_status
  ADD FOREIGN KEY (driver_id) REFERENCES driver (id);
ALTER TABLE orders_status
  ADD FOREIGN KEY (status_id) REFERENCES orders_status_mapping (id);
CREATE INDEX status ON orders_status (status_id);
CREATE UNIQUE INDEX unique_name ON orders_status_mapping (name);
ALTER TABLE rout_point_mapping
  ADD FOREIGN KEY (routine_id) REFERENCES routine (id);
ALTER TABLE rout_point_mapping
  ADD FOREIGN KEY (point_id) REFERENCES point (id);
CREATE UNIQUE INDEX unique_id ON routine (id);
ALTER TABLE sits
  ADD FOREIGN KEY (transport_id) REFERENCES transport (id);
ALTER TABLE sits
  ADD FOREIGN KEY (trip_id) REFERENCES trip (id);
ALTER TABLE transport_props
  ADD FOREIGN KEY (transport_id) REFERENCES transport (id);
ALTER TABLE trip
  ADD FOREIGN KEY (transport_id) REFERENCES transport (id);
ALTER TABLE trip
  ADD FOREIGN KEY (driver_id) REFERENCES driver (id);
ALTER TABLE trip
  ADD FOREIGN KEY (routine_id) REFERENCES routine (id);
CREATE UNIQUE INDEX unique_id ON user (id);
ALTER TABLE user_contact
  ADD FOREIGN KEY (user_id) REFERENCES user (id);
CREATE UNIQUE INDEX unique_id ON user_contact (id);

SET FOREIGN_KEY_CHECKS = 1;