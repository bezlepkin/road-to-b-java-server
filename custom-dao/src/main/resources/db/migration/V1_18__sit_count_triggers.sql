DELIMITER $$
DROP TRIGGER IF EXISTS after_update_sits $$
CREATE TRIGGER after_update_sits
AFTER UPDATE ON sits
FOR EACH ROW
  BEGIN
    DECLARE free_count INTEGER;
    SET free_count = (SELECT count(id)
                      FROM sits
                      WHERE trip_id = old.trip_id AND user_id IS NULL);

    UPDATE trips
    SET sits_free = free_count
    WHERE trips.id = old.trip_id;

  END $$

DROP TRIGGER IF EXISTS after_delete_sits$$
CREATE TRIGGER after_delete_sits
AFTER DELETE ON sits
FOR EACH ROW
  BEGIN
    DECLARE total_sits INT;
    DECLARE total_free INT;

    SET total_free = (SELECT count(id)
                      FROM sits
                      WHERE trip_id = old.trip_id AND user_id IS NULL);

    SET total_sits = (SELECT count(id)
                      FROM sits
                      WHERE trip_id = old.trip_id);

    UPDATE trips
    SET trips.sits_free = total_free
    WHERE trips.id = old.trip_id;

    UPDATE trips
    SET trips.sits_total = total_sits
    WHERE trips.id = old.trip_id;


  END $$


DROP TRIGGER IF EXISTS after_insert_sits$$
CREATE TRIGGER after_insert_sits
AFTER INSERT ON sits
FOR EACH ROW
  BEGIN
    DECLARE total_sits INT;
    DECLARE total_free INT;

    SET total_free = (SELECT count(id)
                      FROM sits
                      WHERE trip_id = new.trip_id AND user_id IS NULL);

    SET total_sits = (SELECT count(id)
                      FROM sits
                      WHERE trip_id = new.trip_id);

    UPDATE trips
    SET trips.sits_free = total_free
    WHERE trips.id = new.trip_id;

    UPDATE trips
    SET trips.sits_total = total_sits
    WHERE trips.id = new.trip_id;
  END $$

DELIMITER ;
