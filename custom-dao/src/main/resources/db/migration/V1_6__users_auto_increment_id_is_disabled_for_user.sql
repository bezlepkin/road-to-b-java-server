SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE road_to_b_dev.users
  MODIFY id INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE road_to_b_dev.users
  ALTER COLUMN is_disabled SET DEFAULT 0;

/*Add user UUID(don't ask why, it is technical issue)*/
ALTER TABLE road_to_b_dev.users
  ADD uuid VARCHAR(36) NOT NULL;

ALTER TABLE road_to_b_dev.user_contacts
  MODIFY id INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE road_to_b_dev.trips
  MODIFY id INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE road_to_b_dev.driver_types
  MODIFY id INT(11) NOT NULL AUTO_INCREMENT;
SET FOREIGN_KEY_CHECKS = 1;