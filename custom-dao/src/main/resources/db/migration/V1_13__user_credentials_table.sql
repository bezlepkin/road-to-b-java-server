CREATE TABLE road_to_b_dev.user_credentials
(
  id          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id     INT             NOT NULL,
  acces_token VARCHAR(36),
  salt        BLOB,
  password    VARCHAR(512),
  last_change TIMESTAMP                DEFAULT CURRENT_TIMESTAMP
);
CREATE UNIQUE INDEX user_credentials_id_uindex ON road_to_b_dev.user_credentials (id);
ALTER TABLE road_to_b_dev.user_credentials
  ADD CONSTRAINT user_credentials_users__fk
FOREIGN KEY (user_id) REFERENCES users (id);