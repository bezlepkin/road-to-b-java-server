SET FOREIGN_KEY_CHECKS = 0;
CREATE TABLE road_to_b_dev.driver_credentials
(
  id        INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  driver_id INT             NOT NULL,
  hash      VARCHAR(512)    NOT NULL,
  salt      VARCHAR(512)    NOT NULL,
  CONSTRAINT driver_identity_driver_id___fk FOREIGN KEY (driver_id) REFERENCES drivers (id)
);
CREATE UNIQUE INDEX driver_identity_id_uindex ON road_to_b_dev.driver_credentials (id);
ALTER TABLE road_to_b_dev.driver_credentials
  COMMENT = 'Driver security model';

ALTER TABLE road_to_b_dev.drivers
  ADD login VARCHAR(128) NOT NULL;
CREATE UNIQUE INDEX drivers_login_uindex ON road_to_b_dev.drivers (login);
SET FOREIGN_KEY_CHECKS = 1;