ALTER TABLE road_to_b_dev.users
  MODIFY uuid VARCHAR(36);
UPDATE users
SET uuid = NULL;
ALTER TABLE road_to_b_dev.users
  CHANGE uuid x_identity BIGINT;
