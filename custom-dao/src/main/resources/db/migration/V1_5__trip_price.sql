SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE road_to_b_dev.trips
  ADD departure_point BIGINT NOT NULL;
ALTER TABLE road_to_b_dev.trips
  ADD arrival_point BIGINT NULL;
/*NEWER!!*/
DELETE FROM trips;
/*Don't do this in production mode - NEWER!*/

ALTER TABLE road_to_b_dev.trips
  ADD CONSTRAINT trips_departure_points__fk
FOREIGN KEY (departure_point) REFERENCES points (id);

ALTER TABLE road_to_b_dev.trips
  ADD CONSTRAINT trips_arrival_points__fk
FOREIGN KEY (arrival_point) REFERENCES points (id);

SET FOREIGN_KEY_CHECKS = 1;