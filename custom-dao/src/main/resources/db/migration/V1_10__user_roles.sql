CREATE TABLE road_to_b_dev.user_roles
(
  id         INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  type       VARCHAR(128),
  notes      VARCHAR(1024),
  is_default TINYINT(4)               DEFAULT 0
);
CREATE UNIQUE INDEX user_roles_id_uindex ON road_to_b_dev.user_roles (id);
ALTER TABLE road_to_b_dev.user_roles
  COMMENT = 'Users acces role';

INSERT INTO road_to_b_dev.user_roles (type, notes, is_default) VALUES ('CUSTOMER', 'Regular user', 1);
INSERT INTO road_to_b_dev.user_roles (type, notes, is_default) VALUES ('MODERATOR', 'Moderate users and drivers', 0);
INSERT INTO road_to_b_dev.user_roles (type, notes, is_default) VALUES ('ADMINISTRATOR', 'Can assign moderators', 0);
INSERT INTO road_to_b_dev.user_roles (type, notes, is_default) VALUES ('SUDO', 'Super User - can do anything', 0);


CREATE TABLE road_to_b_dev.users_roles_mapping
(
  id      INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id INT,
  role_id INT
);
CREATE UNIQUE INDEX users_roles_mapping_id_uindex ON road_to_b_dev.users_roles_mapping (id);

ALTER TABLE road_to_b_dev.users_roles_mapping
  ADD CONSTRAINT users_roles_mapping_users__fk
FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE road_to_b_dev.users_roles_mapping
  ADD CONSTRAINT users_roles_mapping_user_roles_id_fk
FOREIGN KEY (role_id) REFERENCES user_roles (id);